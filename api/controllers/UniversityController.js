/**
 * UniversityController
 *
 * @description :: Server-side logic for managing universities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  'new': function(req, res) {
    res.view();
  },

  create: function(req, res, next) {
    University.create( req.params.all(), function universityCreated(err, university) {
      if (err) return next(err);

      res.redirect('/university/show/' + university.id);
    });
  },

  show: function (req, res, next) {
    University.findOne(req.param('id')).populateAll().exec(function (err, university) {
      if (err) return next(err);
      if (!university) return next();
      res.view({
        university: university
      });
    });

  },

  index: function(req, res, next) {
    University.find(function foundUniversity (err, universities) {
      if (err) return next(err);

      res.view({
        universities: universities
      });
    });
  },

  edit: function(req, res, next) {
    University.findOne(req.param('id'), function foundUniversity(err, university) {
      if (err) return next(err);
      if (!university) return next();

      res.view({
        university: university
      });
    });
  },

  update: function(req, res, next) {
    University.update(req.param('id'), req.params.all(), function universityUpdated(err){
      if (err) {
        return res.redirect('/university/edit/' + req.param('id'));
      }

      res.redirect('/university/show/' + req.param('id'));
    });
  },

  destroy: function(req, res, next) {
    University.destroy(req.param('id')).exec( function() {
      res.redirect('/university/');
    });
  },

  findUniversitybyName:function(req,res)
  {
    var id = req.param('id');
    University.findOne({name:id})
        .exec(function(err,user){

          if(err)
            res.json({error:err});
          if(user === undefined)
            res.json({notFound:true});
          else
            res.json({notFound:false,userData:user});
          
        });
  }

};
