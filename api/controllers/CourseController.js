/**
 * CourseController
 *
 * @description :: Server-side logic for managing courses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  'new': function(req, res, err) {
    University.findOne(req.param('owner'), function foundUniversity (err, university) {
      if (err) return next(err);
      if (!university) return next();
      res.view({
        university: university
      });
    });
  },

  create: function(req, res, next) {
    Course.create(req.params.all(), function courseCreated(err, course) {
      if (err) return next(err);

      res.redirect('/university/show/' + course.owner);
    });
  },

  findCoursebyName:function(req,res)
  {
    var id = req.param('id');
    Course.findOne({name:id})
        .exec(function(err,user){

          if(err)
            res.json({error:err});
          if(user === undefined)
            res.json({notFound:true});
          else
            res.json({notFound:false,userData:user});
          
        });
  }

};