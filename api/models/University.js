/**
 * University.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },

    type: {
      type: 'string'
    },

    rank: {
      type: 'integer'
    },

    estd_year: {
      type: 'integer'
    },

    courses: {
      collection: 'course',
      via: 'owner'
    }

  }
};
